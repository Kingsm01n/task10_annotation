package customClass;

import myAnnotations.CustomAnnotation;

public class Foo {
    @CustomAnnotation(age = 5)
    int age = 5;
    int fAge = 0;

    public int getAge() {
        return age;
    }

    public int getfAge() {
        return fAge;
    }
}
