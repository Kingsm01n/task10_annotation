package MVCpattern;

import customClass.Foo;
import myAnnotations.CustomAnnotation;

@CustomAnnotation(age = 5)
public class Controller {
    public static void annotation(){
        Foo foo = new Foo();
        View.display(foo.getAge() + " " + foo.getfAge());
        View.display(CustomAnnotation.class.getDeclaredFields());
    }
}
