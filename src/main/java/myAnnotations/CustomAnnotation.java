package myAnnotations;

public @interface CustomAnnotation {
    int age() default 5;
}
